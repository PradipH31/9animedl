import { Component, OnInit, NgModule } from '@angular/core';
import { Router } from '@angular/router';

import { AppComponent } from '../app.component';

import { _9Anime } from '../util/_9Anime.util';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.scss']
})

export class DiscoverComponent implements OnInit {
  series = [];

  constructor(
    private _9anime: _9Anime,
    private router: Router,
    public appComponent: AppComponent
  ) { }

  ngOnInit() {
    this.appComponent.isBusy = true;
    this._9anime.discoverAnime().then((res) => {
      this.series = res;
      this.appComponent.isBusy = false;
    });
  }

}
