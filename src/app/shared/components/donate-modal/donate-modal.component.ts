import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { BsModalService, ModalDirective, BsModalRef } from 'ngx-bootstrap/modal';
import { AppComponent } from '../../../app.component';

@Component({
  selector: 'app-donate-modal',
  templateUrl: './donate-modal.component.html',
  styleUrls: ['./donate-modal.component.scss']
})


export class DonateModalComponent implements OnInit {
  @ViewChild('donateModal', {static: true}) modal: any;
  modalRef: BsModalRef;

  constructor(
    private modalService: BsModalService,
    private appComponent: AppComponent
  ) {}

  ngOnInit() {
    this.modalRef = this.modalService.show(this.modal, {
      backdrop: true,
      class: 'modal-lg',
      ignoreBackdropClick: true
    });
  }

  public hideModal(choice: string) {
    if(choice == 'go') {
      this.appComponent.doDonate();
    }

    localStorage.setItem('donation', choice);
    this.modalRef.hide();
    this.appComponent.nagDonation = false;
  }
}
