import { Component, OnInit, Input, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

import { Waf } from '../../../util/Waf.util';
import { _9Anime } from '../../../util/_9Anime.util';


@Component({
  selector: 'app-waf-captcha',
  templateUrl: './waf-captcha.component.html',
  styleUrls: ['./waf-captcha.component.scss']
})


export class WafCaptchaComponent implements OnInit {
  @Input() siteKey: any;
  public waf = new Waf(this.httpClient, new _9Anime(this.httpClient));

  constructor(
    private httpClient: HttpClient,
    private _9anime: _9Anime
  ) {}

  ngOnInit() {
    $("#wafModal").show();
  }

  async onResolve(token) {
    let resp = await this.waf.submitWaf(token);
    
    this._9anime.isWafActive = false;
  }

}
