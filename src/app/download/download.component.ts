import { Component, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

import { Logger } from '../util/Logger.util';
import { Time } from '../util/Time.util';
import { SeriesDB } from '../util/SeriesDB.util';
import { AsyncForEach } from '../util/AsyncForEach.util';
import { Extractor } from '../util/Extractor.util';

import { DownloadTabService } from '../download-tab.service';
import { ToastrService } from 'ngx-toastr';
import { AppComponent } from '../app.component';
import { _9Anime } from '../util/_9Anime.util';
import { ByteConv } from '../util/ByteConv.util';
import { EventEmitter } from 'events';

const fsp = require('fs').promises;
const fs = require('fs');
const electron = require('electron');
var path = require('path');
const request = require('request');

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})

export class DownloadComponent {
  download;
  time = new Time();
  logger = new Logger();

  constructor(
    private appComponent: AppComponent,
    private _9anime: _9Anime,
    private seriesDb: SeriesDB,
    private asyncForEach: AsyncForEach,
    public downloadTabService: DownloadTabService,
    private extractor: Extractor,
    public router: Router,
    public toastr: ToastrService
  ){
    // Get the requested index
    let index: number;
    try {
      index = this.router.getCurrentNavigation().extras.state.index;
    } catch(e) {
      this.toastr.error(`Whoops, could not find any data for the series... sending you back to the discovery...`, "Oh boii");
      this.router.navigate(['discover']);
    }

    // Initialize all service values
    this.download = this.downloadTabService.getDownload(index);
    if(!this.download.isInitialized) {
      this.download.isDownloading = false;
      this.download.episodes = [];
      this.download.promises = [];
      this.download.fileExample = "animeName - S01E01.mp4";
      this.download.animeName = this.download.serie.name;
      this.download.season = 1;
      this.download.progress = {
        total: 0,
        recieved: 0,
        progress: 0,
        canNext: true,
        episode: null,
        filename: null,
        error: false,
        errors: 0
      };
      this.download.status = "Waiting for user...";
      this.download.isInitialized = true;

      // Get the episodes
      let getEpisodes = this.getEpisodes();
      this.download.promises.push(getEpisodes);

      // Get the download directory
      this.getDownloadDir();
    }

    // Wait for everything to init properly
    Promise.all(this.download.promises).then(() => {
      // Add the filename example
      this.formatName();

      // Hide the loading screen
      Promise.resolve(null).then(() => this.appComponent.isBusy = false);
    });
  }

  public byteConv = (new ByteConv).convert;

  public getDownloadDir() {
    // Get the download dir from the settings
    if(localStorage.getItem('download folder')) {
      this.download.downloadDir = localStorage.getItem('download folder');
      return;
    }

    // Use the default
    this.download.downloadDir = electron.remote.app.getPath('downloads');
  }

  public async getEpisodes() {
    // Get the if of the anime
    let animeId = this._9anime.getAnimeId(this.download.serie.url);

    // Build the api url
    let apiUrl = `https://${this._9anime._9AnimeHost}/ajax/film/servers/${animeId}?ts=${this.time.getUnixTimestamp()}&_=728`;
    this.logger.log(`api URL: ${apiUrl}`);

    // Get he json from the api
    // Deserialize it afterwards
    let json = await this._9anime.getHttpContent(apiUrl);
    let data = this._9anime.htmlParser.parseFromString(JSON.parse(json).html, "text/html");
    
    // Get the server id for the preferred server
    //let serverId = this._9anime.getServerId("Mp4Upload");
    let serverId = this._9anime.getServerId("Mp4Upload");

    // Get the episodes list on the Mp4Upload mirror
    // TODO: get the episodes list for Mp4Upload as well
    let episodes = await this._9anime.getEpisodesList(data, serverId);


    // Clear the old episodes list
    // Loop over each of the episodes
    // Add them to the final episodes list
    this.download.episodes = []; 
    episodes.forEach(episode => {
      // Add the episode to the list
      this.download.episodes.push({
        episode: episode.episode,
        id: episode.id,
        server: episode.server,
        download: true
      });
    });
  }

  public toggleEpisode(episode, state) {
    // Update the download flag
    episode.download = state;
  }

  public formatName(episode: number = 1) {
    // Sanitize the anime name
    this.download.animeName = this.download.animeName.replace(/[/\\?%*:|"<>]/g, '');
    
    // Pad the season with zeroes
    let seasonPadded = String(this.download.season).padStart(2, '0');

    // Pad the episode with zeroes
    let epstring = String(episode).padStart(2, '0');

    // Build the example
    let fileExample = `${this.download.animeName} - S${seasonPadded}E${epstring}.mp4`;

    // Return the value
    this.download.fileExample = fileExample;
    return `${this.download.animeName} - S${seasonPadded}E${epstring}`
  }

  public async browseDir() {
    const { dialog } = electron.remote;
    let res = await dialog.showOpenDialog({
      properties: ['openDirectory']
    });

    // Do absolutely nothing if the dialog got closed
    if(res.canceled) {
      return;
    }

    // Set the file path
    this.download.downloadDir = res.filePaths[0];
  }

  public async scanDir() {
    // Create the destination folder name for the anime
    let dest = path.join(this.download.downloadDir, this.download.animeName);

    // Check if the destination folder exists
    try {
      await fsp.stat(dest)
    } catch (err) {
      if (err.code === 'ENOENT') {
        this.logger.log('Directory doesn\'t exist!');
      }
      return;
    }

    // Get the path to the db file
    let db = path.join(dest, '9animedl.json');

    // Check if the db file exists
    try {
      await fsp.stat(db)
    } catch (err) {
      if (err.code === 'ENOENT') {
        this.logger.log('file doesn\'t exist!');
      }
      return;
    }

    // Read the db file
    // Then parse it
    let content = await fsp.readFile(db, 'utf-8');
    let data = this.seriesDb.parse(content);

    // Check all the episodes

    await this.asyncForEach.loop(this.download.episodes, async episode => {
      // Check the episode
      let res = await this.seriesDb.check(episode, data.data, {
        animeName: this.download.animeName,
        season: (<HTMLInputElement>document.getElementById('animeSeason')).value,
        dir: dest
      });

      // Set the value
      episode.download = res;
    })

    // Scan completed
    // TODO: show a toast
    this.logger.log(`Scan completed`);
  }

  public async doDownload() {
    // Disable all the inputs
    this.download.isDownloading = true;

    // Update the download folder in the settings if need be
    if(this.download.downloadDir == "") {
      this.toastr.warning(`Download dir cannot be empty, defaulting to "${this.download.downloadDir}"`);
      this.getDownloadDir();
    }

    if(localStorage.getItem('download folder') != this.download.downloadDir) {
      localStorage.setItem('download folder', this.download.downloadDir);
    }

    let self = this;

    // Only get the episodes we want to download
    let queue = this.download.episodes.filter((episode) => {
      return episode.download;
    }) || [];

    self.toastr.info(`Starting the download of ${queue.length} episode(s) of ${self.download.animeName}...\r\nHang on tight!`, 'Let\s Go!');
    self.download.status = "Preparing...";

    await this.asyncForEach.loop(queue, async episode => {
      // Set some values
      self.download.progress.episode = episode;
      self.download.progress.canNext = false;
      self.download.progress.filename = self.formatName(episode.episode) + '.mp4';

      // Get the embed url
      self.download.status = "Extracting embed url...";
      let embed = await this._9anime.getEmbed({
        id: episode.id,
        server: episode.server
      });

      // Check if we found an embed url
      if(!embed) {
        self.logger.log(`Could not find embed url for ${self.download.animeName} episode #${episode.episode}!`);
        self.toastr.error(`Something went wrong while trying to extract the embed url for ${self.download.animeName} episode #${episode.episode}...`, 'Whoops...');
        return;
      }

      // Get the actual download url
      self.download.status = "Extracting download url...";
      let downloadUrl = await self.extractor.extract({
        url: embed,
        server: episode.server
      });

      // Check if we found a download url
      if(!downloadUrl){
        self.toastr.error(`Something went wrong while trying to extract the download url for ${self.download.animeName} episode #${episode.episode}...`, 'Whoops...');
        self.logger.log("Unable to extract the download url for ${self.download.animeName} episode #${episode.episode}!");
        return;
      }

      self.logger.log(`Download URL for ${self.download.animeName} episode #${episode.episode}: ${downloadUrl}`);
      
      // Download the file
      // TODO: Fix bug with SSL
      self.download.status = "Starting Download...";
      self.download.promises.push(new Promise(function(resolve, reject){
        var req = request({
          method: 'GET',
          uri: downloadUrl,
          strictSSL: false
        });

        // Check if the directory exists
        // Create it if it doesn't
        let dir = path.join(self.download.downloadDir, self.download.animeName);
        if(!fs.existsSync(dir)) {
          fs.mkdirSync(dir, { recursive: true });
        }

        let filepath = path.join(dir, self.download.progress.filename);
        self.logger.log(filepath);
        req.pipe(fs.createWriteStream(filepath));
      
        req.on('response', function ( data ) {
          // Change the total bytes value to get progress later.
          self.download.progress.total = parseInt(data.headers['content-length']);
          self.download.status = "Downloading...";
        });

        req.on('data', function(chunk) {
          // Update the received bytes
          self.download.progress.recieved += chunk.length;
          self.download.progress.progress = Number(((self.download.progress.recieved / self.download.progress.total) * 100).toFixed(0));
        });

        req.on('end', function() {
          self.logger.log(`Episode ${episode.episode} of ${self.download.animeName} succesfully downloaded`);
          self.toastr.success(`Episode ${self.download.progress.episode.episode} of ${self.download.animeName} has been downloaded!`, 'Success');
          self.download.progress.canNext = true;
          resolve();
        });

        req.on('error', function(err) {
          self.toastr.error(`Episode ${self.download.progress.episode.episode} of ${self.download.animeName} failed to download...`, 'Whoops...');
          reject(err);
        })
      })
      .catch(err => {
        this.logger.log(`Failed to download episode ${episode.episode} of ${self.download.animeName}!\r\n${err}`);
        self.download.progress.error = true;
        self.download.progress.canNext = true;
        self.download.progress.errors++;
      }));

      // Wait till download is complete so we can continue
      await new Promise(resolve => {
        function checkCanNext() {
          if (self.download.progress.canNext === true) {
            resolve();
          } else {
            window.setTimeout(checkCanNext, 500); 
          }
        }
        checkCanNext();
      });

      // Read the seriesDb
      // If not existing yet, create it instead
      let file = path.join(self.download.downloadDir, self.download.animeName, '9animedl.json');
      let db;
      if(!fs.existsSync(path.join(file))) {
        db = await self.seriesDb.create();
      } else {
        db = fs.readFileSync(path.join(file));
        db = this.seriesDb.parse(db);
      }

      // update the db
      db = await this.seriesDb.update(episode, db, {
        season: self.download.season,
        filename: self.download.progress.filename,
        success: !self.download.progress.error
      });

      // Write it back to the file
      fs.writeFileSync(file, JSON.stringify(db));

      // Mark the episode as downloaded
      episode.download = self.download.progress.error;

      // Reset all the progress
      self.download.progress.recieved = 0;
      self.download.progress.total = 0;
      self.download.progress.progress = 0;
      self.download.progress.error = false;
    });

    // Check whether we need to show the donation modal
    this.appComponent.nagDonation = self.checkDonate();

    // Inform the user download has completed
    self.toastr.info(`All selected episodes for ${self.download.animeName} have been downloaded (${self.download.progress.errors} errors while downloading)`, 'All done!');
    self.download.status = "Finished Downloading! Waiting for user...";

    // Re-enable all the inputs
    this.download.isDownloading = false;
  }

  public checkDonate() {
    // Check if we've already showed the modal
    // Show if if we haven't yet
    if(!localStorage.getItem('donation')) {
      return true;
    }

    // Check if the user has greeded
    // Hide the modal if they did
    if(localStorage.getItem('donation') == 'greed') {
      this.logger.log('User is greedy :<');
      return false;
    }

    // Check if the user wants to be reminded
    // Show the modal every 5 downloads
    if(localStorage.getItem('donation') == 'remind') {
      let counter = localStorage.getItem('donation counter');
      if(counter == '4') {
        // Reset the counter
        localStorage.setItem('donation counter', '0');
        return true;
      } else {
        // Increase the counter
        counter = ((Number(counter)) + 1).toString();
        localStorage.setItem('donation counter', counter);
        return false;
      }
    }

    // Check if the user already has clicked to donate
    if(localStorage.getItem('donation') == 'go') {
      return false;
    }

    return true;
  }
}
