import { Component, ChangeDetectorRef } from '@angular/core';
import { ElectronService } from './core/services';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../environments/environment';
import { Router } from '@angular/router';

import { _9Anime } from './util/_9Anime.util';
import { faSearch, faTimes, faMinus, faMinusSquare, faPlusSquare, faCodeBranch, faDonate, faInfo, faCommentAlt } from '@fortawesome/free-solid-svg-icons';
import { app } from 'electron';
import { DownloadTabService } from './download-tab.service';

const win = require('electron').remote.getCurrentWindow();
const { shell } = require('electron');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  searchTimeout = null;
  isBusy = true;
  nagDonation = false;
  isMaximized = win.isMaximized();
  downloads = [];

  faSearch = faSearch;
  faTimes = faTimes;
  faMinus = faMinus;
  faMinusSquare = faMinusSquare;
  faPlusSquare = faPlusSquare;
  faCodeBranch = faCodeBranch;
  faDonate = faDonate;
  faInfo = faInfo;
  faCommentAlt = faCommentAlt;

  constructor(
    public electronService: ElectronService,
    private translate: TranslateService,
    public _9anime: _9Anime,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    public downloadTabService: DownloadTabService
  ) {
    translate.setDefaultLang('en');
    console.log('AppConfig', AppConfig);

    if (electronService.isElectron) {
      console.log(process.env);
      console.log('Mode electron');
      console.log('Electron ipcRenderer', electronService.ipcRenderer);
      console.log('NodeJS childProcess', electronService.childProcess);
    } else {
      console.log('Mode web');
    }

    let self = this;
    win.on('maximize', () => {
      self.isMaximized = true;
      this.cdRef.detectChanges();
    });

    win.on('unmaximize', () => {
      self.isMaximized = false;
      this.cdRef.detectChanges();
    });

    self.downloads = this.downloadTabService.getAllDownloads();
  }

  public openTab(index: number) {
    this.downloadTabService.openTab(index);
  }

  public searchChange(val) {
    // Clear the previous timeout
    clearTimeout(this.searchTimeout);

    // Set a new timeout
    this.searchTimeout = setTimeout(() => {
      // Show the loading page
      this.isBusy = true;

      // Make a quick search
      this._9anime.searchAnime(val).then((res) => {
        this.router.navigate(['search'], { 
          state: { 
            series: res
          } 
        });
        this.isBusy = false;
      });
    }, 300);
  }

  public doDonate() {
    shell.openExternal('https://www.paypal.me/finlaydag33k');
  }

  public maximizeWindow() {
    win.maximize();
  }

  public unmaximizeWindow() {
    win.unmaximize();
  }

  public minimizeWindow() {
    win.minimize();
  }

  public exitApp() {
    win.close();
  }

  public openRepo() {
    shell.openExternal('https://www.gitlab.com/finlaydag33k/9animedl');
  }

  public sendFeedback() {
    shell.openExternal('mailto:contact@finlaydag33k.nl?subject=9AnimeDl%20Feedback');
  }
}
