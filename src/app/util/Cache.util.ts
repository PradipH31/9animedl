import { Injectable } from '@angular/core';
import { Logger } from './Logger.util';
import { Time } from './Time.util';

@Injectable({
  providedIn: 'root'
})

export class Cache {
  logger = new Logger();
  time = new Time();

  public set(name: string, data: any, expiry: number = 5) {
    this.logger.log(`Writing item "${name}" to cache`);
    localStorage.setItem(name, JSON.stringify({
      expiry: this.time.addMinutes(5),
      data: data
    }));
  }

  public get(name: string) {
    this.logger.log(`Retrieving item "${name}" from cache`);
    let cache = localStorage.getItem(name);
    if(cache) {
      return JSON.parse(cache).data;
    }

    return null;
  }

  public check(name: string) {
    this.logger.log(`Checking expiry of cache item "${name}"`);
    let cache = localStorage.getItem(name);

    if(cache) {
      let data = JSON.parse(cache);
      if(data.expiry >= this.time.getUnixTimestamp()) {
        this.logger.log(`Cache item "${name}" is still valid`);
        return true;
      }
    }

    this.logger.log(`Cache item "${name}" is no longer valid`);
    return false;
  }
}