import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class Time {
  public getUnixTimestamp() {
    return Date.now() / 1000;
  }

  public addMinutes(minutes: number) {
    return (Date.now() + (minutes * 60_000)) / 1000;
  }
}