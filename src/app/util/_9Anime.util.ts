import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { Waf } from './Waf.util';
import { Time } from './Time.util';
import { Logger } from './Logger.util';
import { Cache } from './Cache.util';

@Injectable({
  providedIn: 'root'
})

export class _9Anime {
  public htmlParser = new DOMParser();
  public waf = new Waf(this.httpClient, this);
  public isWafActive = false;
  public siteKey = "";
  public time = new Time();
  public _9AnimeHost = "9anime.to";
  public servers = {
    "Mp4Upload": 35,
    "F5 - HQ": 36
  };
  private logger = new Logger();
  private cache = new Cache();

  constructor(
    private httpClient: HttpClient
  ) {}

  public async getHttpContent(url, isWafCheck: boolean = false): Promise<string> {
    // Check whether this request is for the waf check
    if(!isWafCheck) {
      // We're not checking for the waf already
      // Check if the waf is active
      this.isWafActive = await this.waf.checkWaf();
      if(this.isWafActive) {
        // Wait for the waf to be solved by the user
        this.logger.log('WAF detected!');
        await this.waf.waitForWaf(this.isWafActive);
        this.logger.log('WAF is no longer holding us up!');
      }
    }

    this.logger.log(`Sending request to ${url}`);

    let resp = await this.httpClient.get(
      url, {
        responseType: 'text'
      }
    );
    return resp.toPromise();
  }

  public async discoverAnime(): Promise<Array<Object>> {
    // Create a new array
    let seriesData = [];

    // Check if we have a cached version
    if(this.cache.check('discovery cache')) {
      return this.cache.get('discovery cache');
    }

    // Get the content of the homepage
    // Turn the content into a DOM node
    let res = await this.getHttpContent(`https://${this._9AnimeHost}/anime`);
    var html = this.htmlParser.parseFromString(res, "text/html");

    // Extract all the series from the DOM node
    let series = html.querySelectorAll("div.widget.hotnew.has-page div.widget-body div.content div.page div.film-list div.item div.inner");

    // Loop over all the series
    series.forEach(serie => {
      // Get the HTML content for the current serie
      let innerHtml = this.htmlParser.parseFromString(serie.innerHTML, "text/html");

      // Get the episodes label
      let episodeElement = innerHtml.querySelector("div.ep") ||
                           innerHtml.querySelector("div.special") ||
                           innerHtml.querySelector("div.movie") ||
                           innerHtml.querySelector("div.ova") ||
                           innerHtml.querySelector("div.preview");
      let episodeLabel = episodeElement.textContent.trim() || episodeElement.nodeValue.trim() || "N/A";
      
      // Add the serie to the data
      seriesData.push({
        name: innerHtml.querySelector("a.name").textContent.trim() || "???",
        url: innerHtml.querySelector("a.name").getAttribute("href").trim() || "",
        thumbnail: innerHtml.querySelector("img").getAttribute("src").trim() || "",
        episodes: episodeLabel 
      });
    });

    // Update our cache
    this.cache.set('discovery cache', seriesData);

    // Return the series data
    return seriesData;
  }

  public async searchAnime(query: string = ''): Promise<Array<Object>> {
    let seriesData = [];

    // Get the content of the search page
    let res = await this.getHttpContent(`https://${this._9AnimeHost}/search?keyword=${query}`);
    let html = this.htmlParser.parseFromString(res, "text/html");
    let series = html.querySelectorAll("div.film-list div.item div.inner");

    series.forEach(serie => {
      // Get the episodes label
      let episodeElement = serie.querySelector("div.ep") ||
                           serie.querySelector("div.special") ||
                           serie.querySelector("div.movie") ||
                           serie.querySelector("div.ova") ||
                           serie.querySelector("div.preview");
      let episodeLabel = episodeElement.textContent.trim() || episodeElement.nodeValue.trim() || "N/A";

      seriesData.push({
        name: serie.querySelector("a.name").textContent.trim() || "???",
        thumbnail: serie.querySelector("img").getAttribute("src").trim() || "",
        url: serie.querySelector("a.name").getAttribute("href").trim() || "",
        episodes: episodeLabel
      });
    });
    
    return seriesData;
  }

  public getAnimeId(url: string) {
    let regex = /https:\/\/.+\/.+\.([a-z0-9]+)/;
    try {
      let res = url.match(regex);
      return res[1];
    } catch (e) {
      return null;
    }
  }

  public getServerId(name: string): number {
   return this.servers[name];
  }

  public async getEpisodesList(data: Document, serverId: number) {
    // Create an array that will hold the episodes list
    let episodesList = [];

    // Get the episodes for the specified serverId
    let links = data.querySelectorAll(`div[data-name="${serverId}"] li>a`);

    // Loop through all the episodes found for the mirror
    links.forEach(episode => {
      // Check if the current episode if uncensored or not
      // This code has not been tested (just a plain re-write from the legacy 9AnimeDl)
      // TODO: Find anime with both censored and uncensored episodes to test
      if(!episode.getAttribute("data-comment").includes("uncen")) {
        // The current episode does not seem to be uncensored
        // Check if an uncensored version is available
        let uncensored = this.getUncensoredEpisode(episode.getAttribute("data-comment"), serverId, data);

        if(uncensored != "") {
          // Uncensored version has been found
          // Add the uncensored version to the episodes list instead
          episodesList.push({
            episode: episode.getAttribute("data-base"),
            id: uncensored,
            server: serverId
          });
        } else {
          // No uncensored version was found
          // Just add the censored version :(
          episodesList.push({
            episode: episode.getAttribute("data-base"),
            id: episode.getAttribute("data-id"),
            server: serverId
          });
        }
      }
    });

    return episodesList;
  }

  private getUncensoredEpisode(episode: string, serverId: number, data: Document) {
    // Select all uncensored links for this episode and server
    // Should (in theory) only have one though
    let uncensoredLinks = document.querySelectorAll(`div[data-name="${serverId}"] li>a[data-comment$="${episode}uncen"]`);

    // Check if a result has been found
    if(uncensoredLinks.length >=1) {
      // A result has been found
      // Get the episode id and return it
      return uncensoredLinks[0].getAttribute("data-id");
    }

    // No uncensored link has been found
    // Return an empty string
    return "";
  }

  public async getEmbed(opts) {
    // Build the episodeUrl
    let episodeUrl = `https://${this._9AnimeHost}/ajax/episode/info?ts=${this.time.getUnixTimestamp()}&_=728&id=${opts.id}&server=${opts.server}`;
    this.logger.log(`episode url: ${episodeUrl}`);

    // Get the JSON
    let resp = await this.getHttpContent(episodeUrl);
    
    // Get the target
    let data = JSON.parse(resp);
    let embed = data.target;
    this.logger.log(`Embed URL: ${embed}`);

    // Return the result
    return embed;
  }
}