import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Logger } from './Logger.util';

const fs = require('fs').promises;
var path = require('path');

@Injectable({
  providedIn: 'root'
})

export class SeriesDB {

  public constructor(
    private logger: Logger
  ) {}

  public parse(data: string) {
    return JSON.parse(data);
  }

  /*
   * Returns true if episode needs to be downloaded
   */
  public async check(episode, db, opts) {
    // Check if we have an entry for the season
    if(db[opts.season] == null) {
      this.logger.log(`Season "${opts.season}" has no downloaded episodes yet.`);
      return true;
    }

    // Check whether the episode exists
    if(db[opts.season][episode.episode] == null) {
      this.logger.log(`Episode "${episode.episode}\ has not been downloaded yet.`);
      return true;
    }

    // Check if the episode was already downloaded successfully
    if(db[opts.season][episode.episode].Success == false) {
      this.logger.log(`Episode "${episode.episode}\ failed to download prior.`);
      return true;
    }

    // Check if the file exists
    let dest = path.join(opts.dir, db[opts.season][episode.episode].FileName);
    try {
      await fs.stat(dest)
    } catch (err) {
      if (err.code === 'ENOENT') {
        this.logger.log(`Episode "${episode.episode}" does not exist in the download location.`);
      }
      return true;
    }

    // The current episode has already been downloaded
    this.logger.log(`Episode "${episode.episode}" has already been downloaded.`);
    return false;
  }

  public async update(episode, db, opts) {
    // Add the season if need be
    if(db.data[opts.season] == null) {
      db.data[opts.season] = {}
    }

    // Add the episode
    db.data[opts.season][episode.episode] = {
      EpisodeId: episode.id,
      FileName: opts.filename,
      Success: opts.success
    }

    // Return the db
    return db;
  }

  public async create() {
    return {
      version: 1,
      data: {}
    };
  }
}