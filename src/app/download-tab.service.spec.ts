import { TestBed } from '@angular/core/testing';

import { DownloadTabService } from './download-tab.service';

describe('DownloadTabService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DownloadTabService = TestBed.get(DownloadTabService);
    expect(service).toBeTruthy();
  });
});
