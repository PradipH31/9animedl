import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  appVersion ;

  constructor() { }

  ngOnInit() {
    this.appVersion = require('electron').remote.app.getVersion();
  }

}
