# 9AnimeDl
Tool to download anime of 9AnimeDl.  
Useful for when you want to take the anime on the go (eg. in a car or plane) or when your connection may not be the best for streaming.  

# Thanks lads!
Because I think it's only fair people get attribution for the work they have delivered, I have a section dedicated to you guys!  
Without the people below, this project would either have not existed or probably be even more unstable than it already is.  

## General thanks
First of all, I want to thank 9Anime for delivering high-quality anime to us for free!  
This project would not have existed without you in the first place!

Second of all, I want to bring out a special thanks to the folks below that helped me out a lot in the project as well in a variety of different ways.  

- Spike2147 (Helping me out with loads of code issues)
- Blake S. (Loyal bug reporter)
- Apollo (Mental support)

Third of all, I want to thank all the contributors of major dependencies used by the project.  
The work of these folks have made making this project a lot easier.  
Please note that these dependencies might also have their own dependencies and as such, I also wish to shout-out to the folks who made this project possible indirectly!  
The list below is in no particular order :)  

- [ElectronJS](https://electronjs.org/)
- [Angular](https://angular.io/)
- [Bootstrap](https://getbootstrap.com/)
- [Puppeteer](https://pptr.dev/)
- [jQuery](https://jquery.com/)
- [NodeJS](https://nodejs.org/en/)
- [Moment](https://momentjs.com/)

If you're not into the list but have made a contribution to the project, don't worry, I have definitely not forgotten about you!  
I probably just didn't have time to add you to the list yet :)

## Financial support
The people below have provided financial support to the project by sending anything you can miss to the following places.  
Make sure to hit me up by mail if you want your name in the list below (along with transaction IDs)!

- [PayPal](https://www.paypal.me/finlaydag33k)
- [Bitcoin](bitcoin:17f77AYHsQbdsB1Q6BbqPahJ8ZrjFLYH2j)
- [Bitcoin Cash](bitcoincash:17f77AYHsQbdsB1Q6BbqPahJ8ZrjFLYH2j)
- Ethereum: TBA (feel free to ask) :)
- More cryptocurrencies available (feel free to ask) :)

This project is built in my own time next to my regular day-job.  
The financial support offered by these people may not be significant but any contribution but in The Netherlands, we have a saying: "alle kleine beetjes helpen" ("all small bits help").  
With the financial support I recieved, I not only help fund this project and my other projects, it also allows me to get back on the rails if something happens to my gear (eg. my harddrive breaks).  

Well... unfortunately, I haven't recieved any financial support yet :\

# A note on my code
This project is a "learn-as-I-go" project.  
This means that the code might not be what a more seasoned developer would like to see and that there might be plenty of bugs in the long run.  
If you are a more seasoned developer, feel free to open a PR or leave feedback so I can improve the code :)  
Thanks for your understanding <3